const eyes = document.querySelectorAll(".point");

document.onmousemove = () => {
  const x = (event.clientX * 100) / window.innerWidth + "%";
  const y = (event.clientY * 100) / window.innerHeight + "%";

  for (const eye of eyes) {
    eye.style.left = x;
    eye.style.top = y;
    eye.style.transform = "translate(-" + x + ", -" + y + ")";
    eye.style.transform = `translate(-${x}, -${y})`;
  }
};
